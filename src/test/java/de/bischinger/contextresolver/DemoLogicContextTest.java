package de.bischinger.contextresolver;

import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import junit.framework.Assert;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * 
 * @author Alex Bischof
 *
 */
@RunWith(Arquillian.class)
public class DemoLogicContextTest
{
	@Deployment
	public static Archive<?> createTestArchive()
	{
		return ShrinkWrap.create(WebArchive.class, "test.war").addPackage(DemoLogic.class.getPackage())
				.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
	}

	@Inject
	@DemoLogicContext
	Instance<DemoLogic> demoLogicInst;

	@Inject
	Context context;

	@Test
	public void test()
	{
		Assert.assertEquals("A", demoLogicInst.get().getString());
		context.setUseOther();
		Assert.assertEquals("B", demoLogicInst.get().getString());
	}
}
