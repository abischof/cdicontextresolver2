package de.bischinger.contextresolver;

import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.BeanManager;

/**
 * @author Alex Bischof
 */
public class DemoLogicProducer
{
	@Produces
	@DemoLogicContext
	public DemoLogic create(BeanManager beanManager, @Any Instance<ContextResolver> contextResolvers)
	{
		return ManagedBeanCreator.createManagedInstance(beanManager, contextResolvers, DemoLogic.class);
	}
}
