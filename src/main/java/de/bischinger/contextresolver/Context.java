package de.bischinger.contextresolver;

import javax.inject.Singleton;

/**
 * 
 * @author Alex Bischof
 *
 */
@Singleton
public class Context
{
	private boolean useB;

	public boolean isUseB()
	{
		return useB;
	}

	public void setUseOther()
	{
		this.useB = true;
	}
}
