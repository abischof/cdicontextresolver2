package de.bischinger.contextresolver;

import javax.enterprise.util.AnnotationLiteral;

/**
 * @author Alex Bischof
 */
public interface ContextResolver
{
	AnnotationLiteral<?> resolveContext(final Class<?> targetClass);
}