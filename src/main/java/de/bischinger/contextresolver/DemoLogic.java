package de.bischinger.contextresolver;

/**
 * 
 * @author Alex Bischof
 *
 */
public interface DemoLogic
{
	public String getString();
}
