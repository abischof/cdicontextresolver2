package de.bischinger.contextresolver;

/**
 * 
 * @author Alex Bischof
 *
 */
@MandantA
public class DemoLogicA implements DemoLogic
{
	@Override
	public String getString()
	{
		return "A";
	}
}
