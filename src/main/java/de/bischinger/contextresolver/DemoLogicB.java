package de.bischinger.contextresolver;

/**
 * 
 * @author Alex Bischof
 *
 */
@MandantB
public class DemoLogicB implements DemoLogic
{
	public String getString()
	{
		return "B";
	}
}
