package de.bischinger.contextresolver;

import javax.enterprise.util.AnnotationLiteral;
import javax.inject.Inject;

/**
 * @author Alex Bischof
 */
public class DemoLogicContextResolver implements ContextResolver
{
	@Inject
	Context context;

	@Override
	public AnnotationLiteral<?> resolveContext(Class<?> targetClass)
	{
		//Determines the context and returns annotionliteral 
		return context.isUseB() ? new MandantB.Literal() : new MandantA.Literal();
	}
}