package de.bischinger.contextresolver;

import java.util.Set;

import javax.enterprise.context.spi.CreationalContext;
import javax.enterprise.inject.Instance;
import javax.enterprise.inject.spi.Bean;
import javax.enterprise.inject.spi.BeanManager;
import javax.enterprise.util.AnnotationLiteral;

/**
 * 
 * @author Alex Bischof
 *
 */
public class ManagedBeanCreator
{
	public static <T> T createManagedInstance(BeanManager beanManager, Instance<ContextResolver> contextResolvers,
			Class<? extends T> clazz)
	{
		//FindFirst
		for (ContextResolver contextResolver : contextResolvers)
		{
			AnnotationLiteral<?> annotationLiteral = contextResolver.resolveContext(DemoLogic.class);
			Set<Bean<?>> beans = beanManager.getBeans(clazz, annotationLiteral);

			//Create CDI Managed Bean
			Bean<?> bean = beans.iterator().next();
			CreationalContext<?> ctx = beanManager.createCreationalContext(bean);
			return (T) beanManager.getReference(bean, clazz, ctx);
		}
		return null;
	}
}
